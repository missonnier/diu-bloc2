# Fiche DIU Bloc 2 : Huffman
### Julien Donet & Fabrice Missonnier

## Consignes pour accéder à la fiche
Cloner le projet GitLab avec la commande
`git clone https://gitlab.com/missonnier/diu-bloc2.git`

## Contenu de ce projet GitLab

Nous avons travaillé en binôme sur le projet. L'analyse de l'algorithme présenté en première partie correspond à un travail de groupe. La partie 2 "Passage à la pratique" correspond au code de Julien. La partie programmation est faite à partir de fonctions et de procédures. La partie 3 "Programmation Objet" correspond au même algorithme d'Huffman, mais programmé par Fabrice en POO (nous voulions voir comment programmer récursivement sur des objets).
![Alt](images/commits.png "Branches et commits")
- Sur la branche master, vous trouverez le code de Julien, ainsi que la fiche ipynb descriptive du projet. Cette fiche est à ouvrir avec Jupyter.
- Sur la branche fab-objet, vous trouverez le code de Fabrice.