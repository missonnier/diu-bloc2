La page que vous recherchez est actuellement indisponible. Le site Web rencontre peut-�tre des difficult�s techniques, ou vous devez modifier les param�tres de votre navigateur. 

--------------------------------------------------------------------------------

Essayez de la mani�re suivante :

Cliquez sur le bouton  Actualiser ou recommencez ult�rieurement.

Si vous avez entr� l'adresse de cette page dans la barre d'adresses, v�rifiez qu'elle est correcte.

Pour v�rifier vos param�tres de connexion, cliquez sur le menu Outils, puis sur Options Internet.. Dans l'onglet Connexions, cliquez sur Param�tres. Les param�tres doivent correspondre � ceux fournis par votre administrateur r�seau ou par votre fournisseur d'acc�s � Internet. 
V�rifiez que vos param�tres de connexion Internet sont d�tect�s. Vous pouvez configurer Microsoft Windows de fa�on � ce qu'il examine votre r�seau et d�tecte automatiquement les param�tres de connexion (si votre administrateur r�seau a activ� ce param�tre). 
Cliquez sur le menu Outils, puis cliquez sur Options Internet. 
Dans l'onglet Connexions, cliquez sur Param�tres r�seau. 
S�lectionnez D�tecter automatiquement les param�tres de connexion, puis cliquez sur OK. 
Certains sites exigent une s�curit� de connexion 128 bits. Cliquez sur le menu ? (Aide) puis sur � propos de Internet Explorer pour d�terminer le niveau de cryptage install�. 
Si vous tentez de joindre un site s�curis�, v�rifiez que vos param�tres de s�curit� le prennent en charge. Cliquez sur le menu Outils, puis cliquez sur Options Internet. Dans l'onglet Avanc�, faites d�filer les options jusqu'� la section de s�curit�, et v�rifiez les param�tres d'utilisation de SSL 2.0, SSL 3.0, TLS 1.0 et PCT 1.0. 
Cliquez sur le bouton  Pr�c�dente pour essayer un autre lien. 



Impossible de trouver le serveur ou erreur DNS
Internet Explorer  
