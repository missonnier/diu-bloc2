import operator
##ajout julien
import ast
from treelib import Node, Tree

############################################################## Version Julien

##Explications
# Ma version contruit un arbre qui contient des noeuds de type "list" et des feuilles de type "dict"

# Une feuille a par exemple le format suivant {'lettre': 's', 'valeur': 2, 'code': '0111'}
# Une feuille représente une lettre, qui a une valeur (nb occurences) et un code binaire correspondant

# Un noeud qui contient les lettres s et i sera codé comme ceci
#  [{'valeur': 4, 'code': '011'}, {'lettre': 'i', 'valeur': 2, 'code': '0110'}, {'lettre': 's', 'valeur': 2, 'code': '0111'}]]]
# La première case est un dict qui a une clef valeur qui est la somme des poids des valeurs des éléments qu'il contient. Il a également un code binaire
# Les deux cases suivantes sont les 2 feuilles (ou noeuds)



def lireFichier(fichier):
    fichier = open(fichier, "r")
    texte = fichier.read()
    fichier.close()
    return texte

def compterNbValeursLettre(texte):
    # création de la table de fréquence des caractères
    dictionnaire = {}
    # on initialise le dictionnaire
    for c in texte:
        dictionnaire[c] = 0
    # on incrémente
    for c in texte:
        dictionnaire[c] += 1

    liste=[]
    
    #faire une liste de dictionnaire
    for key in dictionnaire:
        liste.append(ast.literal_eval("{'lettre':'"+key+"','valeur':"+str(dictionnaire[key])+"}"))  ## utilisation de la meth ast_literal_eval sinon il voit un string à la place d'un dictionnaire

    # And instead of building a new list in memory use stats.iteritems(). The key parameter to the max()
    # function is a function that computes a key that is used to determine how to rank items.
    # Please note that if you were to have another key-value pair 'd': 3000 that this method will only return
    # one of the two even though they both have the maximum value.
    dictionnairetrie = sorted(liste, key=operator.itemgetter('valeur'))
    return dictionnairetrie

def rechercheValeurMini(liste):
    #initialisation du min avec la première valeur
    el = liste[0]
    if(type(liste[0]).__name__=="dict"):
        min = liste[0]['valeur']
    else:
        min = liste[0][0]['valeur']
    
    for element in liste:
        if(type(element).__name__=="dict"):
            val1 = element['valeur']
        else:
            val1 = element[0]['valeur']
        if(val1<min):
            min = val1
            el = element
    return el



def creerArbreValeur(liste):
    while(len(liste)>1): #tant qu'on arrive pas à 1 seul élément racine
        
        # déclaration d'un tableau de 3 cases
        newList=[[0]]*3

        #on récupère la plus petite valeur de la liste
        item1 = rechercheValeurMini(liste)
        if(type(item1).__name__=="dict"): #de type dictionnaire / feuille, on prend direct sa valeur
            val1 = item1['valeur']
        else:                            #de type liste, on prend la valeur du dictionnaire de la première case
            val1 = item1[0]['valeur']
        newList[1] = item1               # La deuxieme case prend le premier item
        liste.remove(item1)              # on le supprime de la liste

        #on récupère la plus petite valeur de la liste
        item2 = rechercheValeurMini(liste)
        if(type(item2).__name__=="dict"):
            val2 = item2['valeur']
        else:
            val2 = item2[0]['valeur']
        newList[2] = item2              # La troisième case prend le second item
        liste.remove(item2)
        
        somme = val1 + val2
        newList[0] = ast.literal_eval("{'valeur':"+str(somme)+"}") #la première case de la liste est la valeur cumulée des dictionaires enfants
        liste.append(newList)
    

    return liste

tree = Tree()

def affecterCodeBinaire(arbre,code,parent):
    if(type(arbre).__name__=='dict'): #une longueur, c'est une feuille
        arbre["code"] = code
        tree.create_node(arbre["lettre"]+" - "+str(arbre["valeur"])+ " ("+str(code)+")",code, parent)
    else:
        tree.create_node(str(arbre[0]["valeur"]) + " ("+str(code)+")",code, parent)
        arbre[0]["code"] = code
        affecterCodeBinaire(arbre[1],code+'0',code) ##le premier élément    
        affecterCodeBinaire(arbre[2],code+'1',code) ##le second élément
            
    return arbre
   
def ajoutCodesBinaires(arbre):
    monArbre=[]
    ##faire un joli arbre
    tree.create_node(str(arbre[0][0]["valeur"]), "root")

    monArbre.append(arbre[0][0])
    monArbre.append(affecterCodeBinaire(arbre[0][1],'0',"root")) #partie gauche de l'arbre
    monArbre.append(affecterCodeBinaire(arbre[0][2],'1',"root")) #partie droite de l'arbre
        
    return monArbre

def creerDictionaireBinaire(arbre,dictionnaire):
    if(type(arbre).__name__=='dict'): 
        if("lettre" in arbre): #On ne prend que les noeuds avec lettre
            dictionnaire[arbre["lettre"]] = arbre["code"]
    else:
        dictionnaire = creerDictionaireBinaire(arbre[1],dictionnaire) ##le premier élément    
        dictionnaire= creerDictionaireBinaire(arbre[2],dictionnaire) ##le second élément
        
    return dictionnaire

def codageMessage(message,dictionnaireBinaire):
    code=""
    for c in message:
        code += dictionnaireBinaire[c]
    return code

def decodageMessage(binaire,dictionnaireBinaire):
    message=""
    codeB=""
    for b in binaire:  ## pour chaque bit
        codeB+=b       ## on concatène avec le précédent pour former le code
        for key,value in dictionnaireBinaire.items():  # on cherche si le code correspond à une valeur du dictionnaire
            if(codeB == value):  # si oui, on prend la lettre associée (key), et on "réinitialise" le code
                message += key
                codeB=""
                break
    return message


# ouverture fichier texte
texte = lireFichier("lorem.txt")

#dictionnaire du nombre de valeurs par lettre
dictionnaireNbValLettre = compterNbValeursLettre(texte)

#construction arbre lettre  / valeur (des feuilles vers la racine)
monArbre = creerArbreValeur(dictionnaireNbValLettre)

# affectation des codes binaires (de la racine vers les feuilles)
monArbreBinaire = ajoutCodesBinaires(monArbre)
#print(monArbreBinaire)

# génération du dictionnaire lettre / code binaire (de la racine vers les feuilles)
dictionnaireBinaire = creerDictionaireBinaire(monArbreBinaire,{})
#print(dictionnaireBinaire)

codeBinaire = codageMessage(texte,dictionnaireBinaire)
#print(codeBinaire)
print ("{0} caractères en compressé, c'est plus court que {1}".format(len(codeBinaire),len(texte)*8))

#test de décodage du message
print(decodageMessage(codeBinaire,dictionnaireBinaire))

##affichage d'un joli arbre
tree.show()
tree.save2file('treeLorem.txt')
